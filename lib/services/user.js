'use strict';

const { Service } = require('schmervice'); 
const Encrypt = require('fl-iut-encrypt');
const { unauthorized } = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = class UserService extends Service {

     create(user) {
          const { User } = this.server.models();
          // ecrypt password
          user.password = Encrypt.encryptSha1(user.password);
          return User.query().insertAndFetch(user);
     }    

     getAllUsers() {
          const { User } = this.server.models();

          return User.query();
     }

     async getUsersMail(idList) {
          const { User } = this.server.models();
          let usersMail = [];
          await Promise.all(idList.map(async (e) => {
               let result = await User.query().select('user.mail').where('id', e.idUser);
               usersMail.push(result[0].mail);
          }));
          return usersMail;
     }

     async deleteById(id) {
          const { User } = this.server.models();

          if (await User.query().deleteById(id)) {
               return "Utilisateur supprimé.";
          } else {
               return "Aucun utilisateur avec cet ID."
          }
     }

     async updateById(id, user) {
          const { User } = this.server.models();

          if (await User.query()
          .findById(id)
          .patch({
               'firstName': user.firstName,
               'lastName': user.lastName,
               'username': user.username,
               'password': Encrypt.encryptSha1(user.password),
               'mail': user.mail,
               'role': user.role
          })) {
               return "Utilisateur mis à jour."
          } else {
               return "Aucun utilisateur avec cet ID."
          }
     }

     async verifyAuthentication(user) {
          const { User } = this.server.models();

          let userFound = await User.query()
          .where('mail', user.mail)
          .where('password', Encrypt.encryptSha1(user.password));

          if (userFound[0] instanceof User) {
               const token = Jwt.token.generate(
                    {
                        aud: 'urn:audience:iut',
                        iss: 'urn:issuer:iut',
                        id: userFound[0].id, 
                        firstName: userFound[0].firstName,
                        lastName: userFound[0].lastName,
                        email: userFound[0].mail,
                        scope: userFound[0].role
                    },
                    {
                        key: 'random_string',
                        algorithm: 'HS512'
                    },
                    {
                        ttlSec: 14400 // 4 hours
                    }
               );
               return token;
          } else {
               return unauthorized();
          }
     }
}
