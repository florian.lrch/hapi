'use strict';

const { Service } = require('schmervice'); 

module.exports = class MovieService extends Service {

    create(movie){
        const { Movie } = this.server.models();

        return Movie.query().insertAndFetch(movie);
    }

    async deleteById(id){
        const { Movie } = this.server.models();
        if(await Movie.query().deleteById(id))
            return "Film supprimé."
        else
            return "Aucun film avec cet ID.";
    }

    async updateById(id, movie) {
        const { Movie } = this.server.models();

        if(await Movie.query()
        .findById(id)
        .patch({
             'title': movie.title,
             'description': movie.description,
             'releaseDate': movie.releaseDate,
             'director': movie.director
        })) {
            return "Film modifié";
        } else {
            return "Aucun film avec cet ID.";
        }
   }

   async verifyMovieExist(id) {
        const { Movie } = this.server.models();
        
        return await Movie.query().findById(id);
   }

}
