'use strict';

const { Service } = require('schmervice'); 

module.exports = class ListService extends Service {
    
    async getUsersWhoHaveMovieInTheirList(idMovie) {
        const { List } = this.server.models();

        return await List.query()
        .select('list.idUser')
        .where('idMovie', idMovie);
    }

    async addToList(idUser, idMovie) {
        const { List } = this.server.models();
        
        let listFound = await List.query()
        .where('idUser', idUser)
        .where('idMovie', idMovie);

        if (!(listFound[0] instanceof List)) {
            if (await List.query().insert({
                idUser: idUser,
                idMovie: idMovie
            })) {
                return "Film ajouté à votre liste !";
            } else {
                return "Erreur lors de l'ajout du film à votre liste.";
            }
        } else {
            return "Vous avez déjà ce film dans votre liste.";
        }
    }

    async removeFromList(idUser, idMovie) {
        const { List } = this.server.models();
        const listFound = await List.query()
        .where('idUser', idUser)
        .where('idMovie', idMovie);

        if (listFound[0] instanceof List) {
            if (await List.query().delete()
            .where('idUser', idUser)
            .where('idMovie', idMovie)) {
                return "Film supprimé de votre liste !";
            } else {
                return "Erreur lors de la suppression du film de votre liste.";
            }
        } else {
            return "Aucun film avec cet ID présent dans votre liste.";
        }
    }

    async deleteUserList(idUser) {
        const { List } = this.server.models();

        return await List.query().delete().where('idUser', idUser);
    }

    async deleteMovieList(idMovie) {
        const { List } = this.server.models();

        return await List.query().delete().where('idMovie', idMovie);
    }


}
