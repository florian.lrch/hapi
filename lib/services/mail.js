'use strict';

const { Service } = require('schmervice'); 
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD
    }
}); 

const from = "catalogue@film.com";

module.exports = class MailService extends Service {
    
    welcomeMail(mail, username) {
        transporter.sendMail({
            from: from,
            to: mail,
            subject: "Bienvenue !",
            text: "Bienvenue " + username + " ! \nMerci d'avoir rejoins notre service ;)",
            html: "<p>Bienvenue " + username + " ! <br>Merci d'avoir rejoins notre service ;)",
        });
    }

    notifyUsersNewMovie(movie, users) {
        users.forEach(user => {
            if (user.role === "user") {
                transporter.sendMail({
                    from: from,
                    to: user.mail,
                    subject: "Nouveau film : " + movie.title,
                    text: "Le film " + movie.title + " vient d'être ajouté à notre catalogue !",
                    html: "<p>Le film " + movie.title + " vient d'être ajouté à notre catalogue !",
                });
            }
        });
    }

    notifyUsersMovieUpdated(mails, movie) {
        mails.forEach(mail => {
            transporter.sendMail({
                from: from,
                to: mail,
                subject: "Mise à jour du film : " + movie.title,
                text: "Le film " + movie.title + " vient d'être modifié dans notre catalogue !",
                html: "<p>Le film " + movie.title + " vient d'être modifié dans notre catalogue !",
            });
        });
    }
}
