'use strict';

const Joi = require('joi')

module.exports = [
    {
        method: 'get',
        path: '/users',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'user', 'admin' ]
            },
        },
        handler: async (request, h) => {

            const { userService } = request.services();

            return await userService.getAllUsers();
        }
    },
    {
        method: 'post',
        path: '/user',
        options: {
            tags: ['api'],
            auth: false,
            validate: {
                payload: Joi.object({
                    firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                    lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                    username: Joi.string().min(3).example('JoDo').description('Username of the user'),
                    password: Joi.string().min(8).example('password').description('Password of the user'),
                    mail: Joi.string().min(8).email().example('johndoe@mail.com').description('Mail of the user')
                })
            }
        },
        handler: async (request, h) => {

            const { userService } = request.services();
            const { mailService } = request.services();

            let userCreated = await userService.create(request.payload);
            mailService.welcomeMail(userCreated.mail, userCreated.username);
            return userCreated;
        }
    },
    {
        method: 'delete',
        path: '/user/{id}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'admin' ]
            },
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {

            const { userService } = request.services();
            const { listService } = request.services();

            listService.deleteUserList(request.params.id);
            return await userService.deleteById(request.params.id);
        }
    },
    {
        method: 'patch',
        path: '/user/{id}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'admin' ]
            },
            validate: {
                payload: Joi.object({
                    firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                    lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                    username: Joi.string().min(3).example('JoDo').description('Username of the user'),
                    password: Joi.string().min(8).example('password').description('Password of the user'),
                    mail: Joi.string().min(8).email().example('johndoe@mail.com').description('Mail of the user'),
                    role: Joi.string().default('user')
                }),
                params: Joi.object({
                    id: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {

            const { userService } = request.services();

            return await userService.updateById(request.params.id, request.payload);
        }
    },
    {
        method: 'post',
        path: '/user/login',
        options: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    mail: Joi.string().min(8).email().example('johndoe@mail.com').description('Mail of the user'),
                    password: Joi.string().min(8).example('password').description('Password of the user')
                })
            }
        },
        handler: async (request, h) => {

            const { userService } = request.services();

            return await userService.verifyAuthentication(request.payload);

        }
    }
]