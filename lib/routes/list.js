'use strict';

const Joi = require('joi');

module.exports = [
    {
        method: 'post',
        path: '/list/addMovieToList/{idMovie}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'user' ]
            },
            validate: {
                params: Joi.object({
                    idMovie: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {

            const { movieService } = request.services();
            const { listService } = request.services();

            if (await movieService.verifyMovieExist(request.params.idMovie)) {
                return await listService.addToList(request.auth.credentials.id,request.params.idMovie);
            } else {
                return "Aucun film existant avec cet ID." 
            }
        }
    },
    {
        method: 'delete',
        path: '/list/removeMovieFromList/{idMovie}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'user' ]
            },
            validate: {
                params: Joi.object({
                    idMovie: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {
            const { listService } = request.services();
            
            return await listService.removeFromList(request.auth.credentials.id, request.params.idMovie);
        }
    }
]