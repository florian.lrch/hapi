'use strict';

const Joi = require('joi').extend(require('@joi/date'));;

module.exports = [
    {
        method: 'post',
        path: '/movie',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'admin' ]
            },
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).example('X-men').description('Title of the movie'),
                    description: Joi.string().min(3).example('X-Men is an American superhero film series based..').description('Description of the movie'),
                    releaseDate: Joi.date().format('DD/MM/YYYY').example('25/05/2015').description('Release date of the movie'),
                    director: Joi.string().min(3).example('Bryan Singer').description('Director of the movie'),        
                })
            }
        },
        handler: async (request, h) => {

            const { movieService } = request.services();
            const { mailService } = request.services();
            const { userService } = request.services();

            mailService.notifyUsersNewMovie(request.payload, await userService.getAllUsers());
            return movieService.create(request.payload);

        }
    },
    {
        method: 'delete',
        path: '/movie/{id}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'admin' ]
            },
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {
            const { movieService } = request.services();
            const { listService } = request.services();

            listService.deleteMovieList(request.params.id);
            return await movieService.deleteById(request.params.id);
        }
    },
    {
        method: 'patch',
        path: '/movie/{id}',
        options: {
            tags: ['api'],
            auth : {
                scope: [ 'admin' ]
            },
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).example('X-men').description('Title of the movie'),
                    description: Joi.string().min(3).example('X-Men is an American superhero film series based..').description('Description of the movie'),
                    releaseDate: Joi.date().format('DD/MM/YYYY').example('25/05/2015').description('Release date of the movie'),
                    director: Joi.string().min(3).example('Bryan Singer').description('Director of the movie'), 
                }),
                params: Joi.object({
                    id: Joi.number().integer().min(0)
                })
            }
        },
        handler: async (request, h) => {
            const { listService } = request.services();
            const { userService } = request.services();
            const { movieService } = request.services();
            const { mailService } = request.services();

            let usersId = await listService.getUsersWhoHaveMovieInTheirList(request.params.id);
            let usersMail = await userService.getUsersMail(usersId);

            mailService.notifyUsersMovieUpdated(usersMail, request.payload);
            
            return await movieService.updateById(request.params.id, request.payload);
        }
    }
]