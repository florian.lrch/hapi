'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('list', (table) => {
            table.integer('idMovie').notNull();
            table.integer('idUser').notNull();
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('list');
    }
};