'use strict';

const Joi = require('joi').extend(require('@joi/date'));;
const { Model } = require('schwifty');

module.exports = class Movie extends Model {

    static get tableName() {
        return 'movie';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            title: Joi.string().min(3).example('X-men').description('Title of the movie'),
            description: Joi.string().min(3).example('X-Men is an American superhero film series based..').description('Description of the movie'),
            releaseDate: Joi.date().format('DD/MM/YYYY').example('25/05/2015').description('Release date of the movie'),
            director: Joi.string().min(3).example('Bryan Singer').description('Director of the movie'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    $beforeInsert(queryContext) {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};