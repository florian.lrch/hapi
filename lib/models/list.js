'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class List extends Model {

    static get tableName() {
        return 'list';
    }

    static get joiSchema() {

        return Joi.object({
            idMovie: Joi.number().integer().greater(0),
            idUser: Joi.number().integer().greater(0),
        });
    }

    $beforeInsert(queryContext) {
    }

    $beforeUpdate(opt, queryContext) {
    }

};