# Iut-project - Florian LAROCHE

## Variables d'environnement

Liste des variables d'environnement à configurer.

Config BD :

Nom d'hôte de MySQL  
DB_HOST=  
Utilisateur de la base de données  
DB_USER=  
Mot de passe associé  
DB_PASSWORD=  
Nom de la base de données utilisée  
DB_DATABASE=  

Config ethereal :

Nom d'utilisateur  
MAIL_USER=  
Mot de passe  
MAIL_PASSWORD=  

Ficher .env à créer dans le dossier /server  

Exemple complet de fichier .env :  
DB_HOST=0.0.0.0  
DB_USER=user1  
DB_PASSWORD=password  
DB_DATABASE=hapi  
MAIL_USER=test.test@ethereal.email  
MAIL_PASSWORD=AZERTY1234azerty  

## Lancement

Clone le projet  

npm install  

Créer / configurer le fichier .env  

npm start  

Tester le projet : http://localhost:3000/documentation  

Pour vérifier les mails se connecter sur le lien suivant : https://ethereal.email/login